---
id: doc2
title: Gerenciamento dos portais 
---

 - Cada prefeitura esta instalada em uma vps e cada vps tem um painel de config.

 - Todas as camaras estão em uma única vps e essa vps tem um unico painel de config.

## TIPOS DE ERRO COMUM

### Portal fora do ar

Caso algum portal esteja fora do ar é necessario entra no painel de config. e verifica os servicos que estão parados e restarta-lo.

forma de acesso (_nome da prefeitura+alfaconsultoria.digital:2087_)

### Sem conexao com o banco 
**_PREFEITURA_** - Ir ate o painel de config. da propria prefeitura e restarta o mysql e caso não resolva sera necessario acesso por ssh a maquina e restart o banco manualmente ou usar a **receita ansible**.

(_systemctl restart mariadb_)


**_CAMARAS_** - O banco das camaras pode ser restartado pelo painel,acessando ssh ou executando a _receita do ansible_. Se a conexão com o banco cair **todas as camaras** estaram fora do ar.

Caso a receita não restart o banco na forma correta ser necessario altera o arquivo "" que esta no camiho "" deixando comentado as linhas e restarta novamente após o funcionamento correto entre no arquivo descomente a parte comentada.


**OBS.:** O suporte tem acesso aos painel em outra porta e com menos permissão que a do time de infra (nome da prefeitura+alfaconsultoria.digital:2083)


### Email da sogo sem conexão

- Conexão ssh na maquina que esta os serviços do email e ver o status de alguns servicos para restarta-lo:

 - **cwpsrv** _(systemctl status cwpsrv / systemctl restart cwpsrv)_
 - **Httpd** _(systemctl status httpd / systemctl restart httpd)_
 - **Nginx** _(systemctl status nginx / systemctl restart nginx)_

**OBS.**: para conexão com a maquina e necessario a chave de o ip.

## PASTA RAIZ DOS PORTAIS

### Path default

 Cada portal tem uma pasta exclusiva porém todas estão em um unico caminho padrão, _independente_ da VPS. Todos estão no seguinte caminho:
 - **/home/NomeDaPrefeitura/public_html**

 A pastas dos orgãos estão no seguinte caminho:
 - **/home/NomeDaPrefeitura/NomeDoOrgão**