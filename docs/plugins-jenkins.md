---
id: jenkins-plugins
title: Plugins que pode ajuda na construção de pipelines
sidebar_label: Instalando Plugins 
---

Acesse o link da documentação oficial para instalar o [Bacula 9.0.8](http://www.bacula.lat/community/comandos-de-compilacao/)

## Estrutura de backup do Bacula

Apos a instalação do bacula teremos quatro arquivo no diretorio **/etc/bacula/**, são eles:

* Bacula-fd.conf
* Bacula-sd.conf
* Bacula-dir.conf
* Bconsole.conf


**Bacula-dir**  
O _Director_ (bacula-dir.conf) é a parte mais complexa do sistema. Nele é possível configurar quase todas as opções do Bacula, como job, agendamentos, pools, fileset, definição do tipo de armazenamento, etc. Enfim, é onde se **configura os clientes** e os arquivos que irão fazer parte da configuração do Bacula.


**Bacula-fd**  
O _File Daemo_ e o arquivo que deve ser instalado no cliente, isto é, onde o bacula ira fazer o backup. Os Clientes do Bacula não têm muitas dependências e desde que eles são desenvolvidos C/C++ eles podem ter seus binários construídos basicamente em qualquer sistema operacional. É importante lembrar que a **versão do cliente não pode ser maior do que a do Director e Storage Daemons**, embora o oposto possa acontecer.

**Bacula-sd**   
Arquivo _Storage Daemo_ responsável por realizar a gravação e restauração dos dados em diversos tipos de dispositivos. Nele é possível configurar a maneira como o storage daemon acessa o **dispositivo de gravação**.

**Bconsole**  
 O _Bacula Console_ (às vezes chamado de User Agent) é um programa que permite ao usuário ou ao administrador do sistema interagir atraves da **interface shell** com o Bacula Director enquanto o daemon está em execução.

 ![image](/img/bacula_diagrama.png)

 O Usuario atraves da interface grafica ou pela linha de comando ira acessa o gerenciado central do bacula chamado de __director__. Esse __director__ ira fazer o backup das pastas no cliente e enviara para o __Storage__, esse estorage ira armazena os backup para futuros restores. O __director__ tambem enviara todos os comandos e ações feita para o __Catalog__ que é um banco exclusivo para esse armazenamento.

## Configuração dos arquivos

### Bacula-dir

O arquivo bacula-dir é o principal arquivo do servido do bacula. nesse arquivo esta todas as configurações. Nesse arquivo é necessario configura os seguintes paramentros:

1. Nome do Director
2. Senhas
3. Cliente
4. Jobs
5. FileSet
6. Pool
7. Storage
8. Catalog
9. Schedule
10. Console

#### Nome e senha
```  
 Director {                            
    Name = bacula-dir  
    DIRport = 9101                
    QueryFile = "/usr/libexec/bacula/query.sql"  
    WorkingDirectory = "/var/spool/bacula"  
    PidDirectory = "/var/run"  
    Maximum Concurrent Jobs = 1  
    Password = "senha-bacula"         
    Messages = Daemon
}
```
É importante ter cuidado com o **_name_** e a **_senha_** do _director_ pois é com esses paramentros que os demais arquivos iram se conectar.

O **_name_** pode ser alterado porém o sufixo precisa ser **_-dir_** para ser identificado como director.

Em **_Maximum Concurrent Jobs_** é colocada a quantidade de jobs que pode ser executado simultaneamente

**Client**
```  
Client {
  Name = "bacula-serve-fd"
  Address = "IP"
  FdPort = 9102
  Password = "bacula"
  Catalog = "MyCatalog"
  FileRetention = 3122064000
  JobRetention = 3122064000
  AutoPrune = yes
}
``` 

**_Name e Password_** – Mesmo name e password que esta no arquivo __bacula-fd.conf__ é atravez desses paramentros que o cliente ira se comunicar com o director.

**_Address_** – IP ou DNS da maquina onde o __bacula-fd.conf__ esta instalado.

**_FileRetention_** – Tempo de retenção para o arquivo não ser excluido. Contado em segundos

**_JobRetention_** – Tempo de retenção para o job não ser excluido. Contato em segundos

**Jobs**
```  
JobDefs {
  Name = "DefaultJob"
  Type = Backup
  Level = Incremental
  Client = bacula-fd
  FileSet = "Full Set"
  Schedule = "WeeklyCycle"
  Storage = File
  Messages = Standard
  Pool = File
  Priority = 10
  Write Bootstrap = "/var/spool/bacula/%c.bsr"
}

Job {
  Name = "IdentificaçãoDoCliente"
  JobDefs = "DefaultJob"
  FileSet = "Nome do file set criado"
  Schedule = "Agenda Semanal"
}

```

**_JobDefs_** – Classe de job padrão, podendo ser utilizado por diversos job’s.

**_Name_** – Nome do JobDefs.

**_Type_** – Tipo padrão (Backup – Restore – Cópia).

**_Level_** – Incremental ( Pré configuração ).

**_Client_** – Cliente.

**_Schedule_** – Agendamento.

**_Storage_** – Tipo de armazenamento Disco | Fita.

**_Messages_** – Mensagens.

**_Pool_** – ( Método de gravação) – Padrão.

**_Priority_** – Prioridade ( 0 é a maior prioridade 100 é a menor ).

**_Write Bootstrap_** – Arquivo temporário que o Bacula cria.

**_Job_** – É um trabalho de backup especifico para um cliente.  
**_Observação_**: Cada parâmetro especificado em um Job, ira sobre escreve em tempo de execução sobre o mesmo parâmetro especificado no JobDfes.

**Storage**
```
Storage {
  Name = "File1"
  SdPort = 9103
  Address = "localhost"
  Password = "bacula"
  Device = "FileChgr1"
  MediaType = "File1"
  Autochanger = "File1"
  MaximumConcurrentJobs = 10
}
```

**_Password_** – Senha do bacula-sd

**_Device_** – tipo de device do storage

**_Media Type_** – Tipo de storage

_Para cada storage (sd) que for armazenar os backups, deverá conter o bacula-sd instalado e configurado conforme o director-dir. A configuração do storage precisa estar igual ao arquivo bacula-sd.conf._

**Catalog**
```
Catalog {
  Name = "MyCatalog"
  Password = ""
  User = "bacula"
  DbName = "bacula"
}
```
Maneira com que o Bacula se conecta no banco de dados dele. Todos os paramentros foi configurado na hora da instalação

**_dbname_** – nome do banco de dados  

**_dbuser_** – usuário que se conecta no banco de  

**_dbpassword_** – senha do banco de dados  

**Schedule**
```
Schedule {
  Name = "Agenda Semanal"
  Run = Level="Full" 1st sun at 6:00
  Run = Level="Incremental" at 6:00
}
Schedule {
  Name = "WeeklyCycle"
  Run = Level="Full" 1st sun at 23:05
  Run = Level="Differential" 2nd-5th sun at 23:05
  Run = Level="Incremental" mon-sat at 23:05
}
```
O “schedule” ou agendamento deve ser associado a um “Job” criado neste arquivo a um agendamento. Por padrão o Bacula  já vem configurado com dois tipos de agendamentos.


Uma para o jobs de backup do servidor do bacula – **_(WeeklyCycle)_**

– Uma para o jobs de backup – **_(WeekyCycleAfterBackup)_**

**_Run_** - É onde faz a configuração do agendamento.

**_Formula_**: "Tipo de backup" + "Data ou dia da semana" + "Horario"

**FileSet**

```  
 FileSet {
  Name = "Full Set"
  Include {
    Options {
      signature = MD5
    }
    File = /usr/sbin
  }

  Exclude {
    File = /var/spool/bacula
    File = /tmp
    File = /proc
    File = /tmp
    File = /.journal
    File = /.fsck
  }
}
``` 

Inclui e excluir diretório/arquivos para serem backupeados. 


Dentro de **_Include_** no paramentro **_File_** sera configurado o path dos arquivos a ser backupeado.

Dentro do **_Exclude_** sera informand o caminho a ser excluido do backup.  A exclusão só terá sentido se existir parte do caminho na de inclusão do backup (diretório/arquivo).

**Pool**
```
Pool {
  Name = "Mensal"
  PoolType = "Backup"
  LabelFormat = "VolMensal-${NumVols}"
  MaximumVolumes = 1000
  MaximumVolumeBytes = 107374182400
  VolumeRetention = 31449600
  VolumeUseDuration = 86400
  AutoPrune = yes
  Recycle = yes
}
Pool {
  Name = "Semanal"
  PoolType = "Backup"
  LabelFormat = "VolSemanal-${NumVols}"
  MaximumVolumes = 1000
  MaximumVolumeBytes = 107374182400
  VolumeRetention = 2332800
  VolumeUseDuration = 259200
  AutoPrune = yes
  Recycle = yes
}
```
Responsável por controlar o tempo que o backup ira fica armazenado, aqui é possível configurar para que o label(volume) seja criado automaticamente.

Pode ser criado varias Pool e configurado no job do cliente.



### Bacula-fd

```
Director {
  Name = bacula-serve-dir
  Password = "bacula"
}

```
**_Name_** e **_Password_** que esta configurado no arquivo **_bacula-dir.conf_**. É com esses parametros que o cliente ira se cominunicar com o servidor bacula.


```
FileDaemon {                          # this is me
  Name = bacula-serve-fd
  FDport = 9102                  # where we listen for the director
  WorkingDirectory = /var/lib/bacula
  Pid Directory = /var/run
  Maximum Concurrent Jobs = 20
  Plugin Directory = /usr/lib64
}
```

Propriedades do cliente.

o **_Name_** do arquivo precisa ser igual ao que esta no arquivo **_bacula-dir.conf_**.

**_Maximum Concurrent Jobs_**: Quantidade de jobs que podera ser executado simultaneo para esse cliente


### bacula-sd

```
Director {
  Name = "bacula-serve-dir"
  Password = "bacula"
}
```
O **_Name_** do arquivo precisa ser igual ao que esta no arquivo **_bacula-dir.conf_** .

```
Storage {
  Name = "bacula-serve-sd"
  WorkingDirectory = "/var/lib/bacula"
  PidDirectory = "/var/run"
  PluginDirectory = "/usr/lib64"
  MaximumConcurrentJobs = 20
}
```
Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac euismod odio, eu consequat dui

```
Device {
  Name = "FileChgr1-Dev1"
  MediaType = "File1"
  ArchiveDevice = "/backup"
  RemovableMedia = no
  RandomAccess = yes
  AutomaticMount = yes
  LabelMedia = yes
  AlwaysOpen = no
  MaximumConcurrentJobs = 5
}
Device {
  Name = "FileChgr1-Dev2"
  MediaType = "File1"
  ArchiveDevice = "/backup"
  RemovableMedia = no
  RandomAccess = yes
  AutomaticMount = yes
  LabelMedia = yes
  AlwaysOpen = no
  MaximumChangerWait = 5
  MaximumOpenWait = 5
  VolumePollInterval = 5
  MaximumRewindWait = 5
  MaximumConcurrentJobs = 5
}
Device {
  Name = "FileChgr2-Dev1"
  MediaType = "File2"
  ArchiveDevice = "/backup"
  RemovableMedia = no
  RandomAccess = yes
  AutomaticMount = yes
  LabelMedia = yes
  AlwaysOpen = no
  MaximumConcurrentJobs = 5
}
```
Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac euismod odio, eu consequat dui
### Bconsole

```
Director {
  Name = bacula-serve-dir
  DIRport = 9101
  address = localhost
  Password = "bacula"
}
```

**_Name_** e **_Password_** que esta configurado no arquivo **_bacula-dir.conf_**. É com esses parametros que o console ira se cominunicar com o servidor bacula.

**address**: IP ou DNS da maquina que o bconsole esta instalado

