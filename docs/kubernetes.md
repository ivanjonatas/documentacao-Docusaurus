---
id: kubernetes
title: Iniciando no kubernetes
sidebar_label: kubernetes para iniciantes
---

![image](/img/k8s_logo.png)

Podemos dividir os estudos do K8s em três formas:

* Componentes
* Objetos
* Ferramenta de automação do cluster


## Componentes  
O cluster e formado por varios componentes que nao pode ser retirados pois ira quebra o cluster.

* A Serve
* Etcd
* Scheduller
* Controller
* Kubelet
* Kube-proxy

**Componentes dentro da estrutura**
![image](/img/structK8S.png)

## Obejtos 
Os objetos são usados para containezar as aplicações e cada objeto tem o seus atributos exclusivos.

* Pods
* Deployments
* Services
* State set full
* ReplicaSet


### Pods

É o menor objeto do kubernetes. É nele que ficara um ou mais containers. O pod supor apenas um container ou varios containers. Entre tanto o pod nao escala a sua plicação nem mante ela funcionando, caso alguem exclua o container o pod nao subira outro container.


```  
arquivo yaml
```
### Deployments

O deployment trabalha com o Controller e o Replica set, ambos são objetos que podem ser usada dentro de um arquivo .yml alem do pods

```  
arquivo yaml
```

### Services

```  
arquivo yaml
```

### State set full

```  
arquivo yaml
```

### ReplicaSet

```  
arquivo yaml
```


## Ferramentas  
Temos varias ferramentas para criar o cluste de forma rapida em varias plataformas de cloud. 

* RKE
* Kubesprays
* Kops
* TK8
* Kubeadm

Contudo todas as maquinas que iram fazer parte do cluster precisa ter instalado as seguintes ferramentas:

* Docker
* Kubectl
* kubelet
* Kubeadm

Comando para instalar o docker
```
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install -y docker-ce
sudo docker --version 

```
Apos instalar o docker coloque seu usuario no grupo docker para não precisa digita o sudo todas as vezes. digite o comando abaixo.

```
sudo usermod -a -G docker $USER

```
apos instalar o docker e coloca seu usuario no grupo docker faça a instalação das demais dependencias.

```
sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl kubelet kubeadm

```


### Kubeadm


**INCIANDO O CLUSTER**

Para criar o cluste com o kubeadm entre na maquina que sera o manager edigite o seguinte comando ```sudo kubeadm init --apiserver-advertise-address $(hostname -i)``` assim ele iniciara o cluster e colocara o ip da maquina como o manager do cluster. 

Esse comando mostrara um log da criação do cluster é nesse log que tera instruçoes para realizar algumas configurações na maquina manager e coloca outras maquinas como worker(nodes).

Primeira instrução a ser seguida:

```  
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config


```

A segunda instução é um script para ser executado na maquina que servira como node

```
sudo kubeadm join 172.31.16.199:6443 --token p309yp.hmsbelyiputru0iz \
    --discovery-token-ca-cert-hash sha256:b1577c74c55eb32e39217e4e2aecbe9063a9c39ff1f33303fee13f502a00d600

```

**NODES SEM COMUNICAÇÃO**

Execute o comando ``` kubectl get podes ``` para ver os nodes cadastrados, porem os nodes ainda não estão configurado, eles apareceram com o status **_NotReady_** isso acontece porque ainda não existe comunicação a nivel de serviços entre nodes e manager. Vamos configurar o cluster adicionando os pods de comunicação. Entre na maquina manager e execute o comando abaixo;

```
sudo kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

```
Confirme se os pods estão em execução digitando o comando ``` kubectl get pods -n kube-system ``` e verificando se os pods estão com os status _**Running**_


Confirme se o cluster esta pronto para ser utilizado verificando se todos os nodes estão como **_Ready_** em seus status, para isso execute o comando ``` kubectl get podes ```

 <!-- ![image](/img/bacula_diagr) -->


## Configuração dos arquivos

## Comandos usados

**COMANDOS DE DESCRIBE**

kubectlscale deployment "nome do deployment" --replicas-10

kubectl describe nodes ip-172-31-0-24 # descrevendo o nodes

kubectl get pods -n kube-system # verificando os pods que contem os componentes do k8s

kubectl describe pod kube-apiserver-ip-172-31-20-159 -n kube-system # informação do pod em outro namespace

kubectl get pods -o wide -n kube-system # verificando onde os pods esta rodando

source <(kubectl completion bash)

porta de cada componentes
MANAGER
api 6443 tcp
etc 2379-2380
kubelet 10250
scheduller 10251
controller 10252
kubelet api read-only 10255 

WORKER
kubelet 10250
kubelet api read-only 10255
nodePort services=> 30000-32767

tres tipos de service
clusterPI
NodePort
loadBalance

criando um service do tipo clusterPI
kubectl expose deployment "nome do deployment"

criando um service do tipo NodePort
kubectl expose deployment "nome do deployment" --type NodePort


#### Nome e senha
```  
arquivo yaml
```