---
id: gitlab
title: Escalando o Gitlab Runner
sidebar_label: Gitlab para iniciantes
---
![image](/img/cigitlab.png)

**Esse é o manual básico de como escalar o gitlab Runner na AWS usando maquinas Spot**

O Gitlab Runner sera escalado usando as _instancias spot_ da aws, sera criado uma quantidade especificada de maquinas spot e as mesmas serão registradas de forma automatica no gitlab. 

Vamos instalar e configurar apenas um Runner Manager, onde gerará novas máquinas Docker sob demanda, assim teremos no gitlab apenas um Runner cadastrado porém "por de baixo dos panos" teremos varias maquinas spot executando as pipelines.

**Após a execução das pipelines as instancias spot serão desativada.**

## Instalando o Runner

O Runner sera instalar dentro do container docker para isso certifique-se de que a maquina tem o **docker instalado.**

Comando para instalar o Runnner:
```
sudo docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

```

## Configurando o Runner

È necessario configurar o Runner com as credencias que o gitlab informa alem de coloca as credencias da aws.

Primeiro vamos cadastra o Runner no Gitlab.

Comando para cadastrar
```
docker exec gitlab-runner gitlab-runner register \
      --non-interactive \
      --executor "docker+machine" \
      --docker-image docker:19.03.5 \ 
      --url "URL do gitlab" \
      --registration-token "TOKEN do gitlab" \
      --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
      --description "docker-runner" \
      --tag-list "docker" \
      --run-untagged="true" \
      --locked="false" \
      --access-level="not_protected" 

```
**Conhecendo alguns paramentros**

**_executor:_** Nesse paramentro define como os jobs serão executado. A configuração acima esta informando os jobs sera o docker e a docker machine(docker+machine), essa opção foi escolhida pois e a docker machine que ira criar as maquinas spot na AWS.

**_url e token:_** Esses paramentros pode ser encontrado na configuração do gitlab na area de Runners.

**_docker-image:_** Imagem default para execução das pipelines.

Após o cadastro do Runner sera necessario entra no arquivo de configurção do mesmo para incluir as credencias da AWS. O arquivo fica dentro do container na pasta  _/etc/gitlab-runner/config.toml_ dentro desse arquivo vamos coloca todas as informações necessaria para criar uma instancia. Abaixo como o arquivo deve ficar o arquivo.

**config.toml**
```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "docker-runner"
  url = "URL do Gitlab"
  token = "Token do Gitlab"
  executor = "docker+machine"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker:19.03.5"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.machine]
    IdleCount = 1                    # - Deve haver 5 máquinas no estado ocioso - quando o modo Off Peak estiver desativado
    IdleTime = 600                   # - 
    MaxBuilds = 3                    # - Cada máquina pode lidar com até 1 trabalhos seguidos (depois disso, ela será removida)
    MachineName = "gitlab-runner-%s"    # - Cada máquina terá um nome exclusivo ('% s' é necess�)
    MachineDriver = "amazonec2"
    MachineOptions = [
      "amazonec2-access-key=AAA",
      "amazonec2-secret-key=AAA",
      "amazonec2-region=us-west-1",
      "amazonec2-zone=c",
      "amazonec2-vpc-id=vpc-AAA",
      "amazonec2-subnet-id=subnet-AAA",
      "amazonec2-security-group=NomeDoGRupo",
      "amazonec2-instance-type=t2.micro",
      "amazonec2-request-spot-instance=true",
      "amazonec2-spot-price=0.05"
    ]
    OffPeakPeriods = ""
    OffPeakIdleCount = 0
    OffPeakIdleTime = 120

```

**Conhecendo alguns paramentros**

**_concurrent:_**

**_MachineOptions:_**

**_IdleCount:_**

**_IdleTime:_**

**_MaxBuilds:_** Quantidade de builds que o runner podera executar

**_MachineName:_**

**_MachineDriver:_**

**_Paramentros Spot:_**

docker exec -it gitlab-runner gitlab-runner run

Link Referencia para documentação

https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/


https://docs.gitlab.com/runner/register/