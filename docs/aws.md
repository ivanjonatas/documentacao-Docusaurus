---
id: aws
title: Iniciando na aws
sidebar_label: AWS para iniciantes
---

Acesse o link da documentação oficial


comandos mais utilizados

aws cloudformation create-stack --template-body file://ec2cloudFormation.yml --stack-name runner

aws cloudformation delete-stack --stack-name cluster-k8s


### ECR COM O AWS CLI

Link com todos os comandos 

https://docs.aws.amazon.com/cli/latest/reference/ecr/index.html

Para conseguir executar todos os comandos é necessario instalar o AWS CLI e configura-lo com suas credencias e coloca sua credencia na mesma região que os repositorios.

**Comando para Cadastra as credencias**
```
aws configure

AWS Access Key ID [****************]:seu Access Key
AWS Secret Access Key [****************]: seu Secret Access
Default region name [us-east-1]: região onde esta seus repositorios
Default output format [None]: opcional, pode deixa com none

```
